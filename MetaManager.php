<?php

namespace Push\MetaManagerBundle;

use Doctrine\ORM\EntityManager;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;

use Push\MetaManagerBundle\Entity\Meta;

class MetaManager
{
    protected $em;
    protected $metaRepo;
    protected $kernel;
    protected $metaSets;

    public function __construct(EntityManager $em, $kernel) 
    {
        $this->em           = $em;
        $this->kernel       = $kernel;
        $this->metaRepo     = $em->getRepository('PushMetaManagerBundle:Meta');
    }

    public function getFieldsByKey($key, $asArray = true, $requiredRole = 'all')
    {
        $output = $this->metaRepo->getFieldsByKey($key, $requiredRole);

        if ($asArray) {
            $output = $this->generateArray($output);
        }

        return $output;
    }

    public function getFieldByKey($key, $field)
    {
        $result = $this->metaRepo->findOneBy(array(
            'metakey'   => $key,
            'field'     => $field,
        ));

        return $result;
    }

    public function getFieldsByRequest(Request $request, $includeGlobal = true)
    {
        $route = $request->get('_route');
        $path = $request->getPathInfo();

        $routeFields = $this->getFieldsByKey($route);
        $pathFields = $this->getFieldsByKey($path);

        $output = array();
        $output['route'] = $routeFields;
        $output['path'] = $pathFields;

        if ($includeGlobal) {
            $output['global'] = $this->getFieldsByKey('global');
        }

        return $output; 
    }

    public function createMeta($key, $field, $label, $type, $value = null, $requiredRole = null)
    {
        if ($this->getFieldByKey($key, $field)) {
            #FIXME should probably return an exection
            return 'Already Exists';
        }

        $meta = new Meta();

        $meta->setMetakey($key);
        $meta->setField($field);
        $meta->setLabel($label);
        $meta->setValue($value);
        $meta->setType($type);
        $meta->setRequiredRole($requiredRole);

        $this->em->persist($meta);
        $this->em->flush();

        return true;
    }

    public function getAllKeys()
    {
        $keys = $this->metaRepo->getAllKeys();
        return array_map('current', $keys);
    }

    public function loadGlobalMeta()
    {
        $path = $this->kernel->getRootDir() . '/config/meta.yml';

        if (file_exists($path)) {
            $this->metaSets = Yaml::parse($path);
        }
    }

    public function loadMetaFromBundles()
    {
        $this->loadGlobalMeta();

        $bundles = $this->kernel->getBundles();
        $allMeta = array();

        foreach ($bundles as $bundle) {
            $path = $bundle->getPath() . '/Resources/config/meta.yml';

            if (file_exists($path)) {
                $contents = file_get_contents($path);
                $meta = Yaml::parse($path);

                if ($meta) {
                    $allMeta = array_merge($allMeta, $meta); 
                }
            }
        }

        foreach ($allMeta as $key => $metas) {
            foreach ($metas as $name => $metaOptions) {
                $role = null;

                if ($name == "_sets") {
                    $this->loadSetsForKey($key, $metaOptions);
                    continue;
                }

                if ($this->getFieldByKey($key, $name)) {
                    continue;
                }

                if (array_key_exists('role', $metaOptions)) {
                    $role = $metaOptions['role'];
                }

                $this->createMeta($key, $name, $metaOptions['options']['label'], $metaOptions['type'], null, $role);
            }
        }
    }

    protected function loadSetsForKey($key, $sets)
    {
        foreach ($sets as $set) {
            $metas = $this->metaSets['set'][$set];

            foreach ($metas as $name => $metaOptions) {
                $role = null;

                if ($name == "_sets") {
                    $this->loadSetsForKey($key, $metaOptions);
                    continue;
                }

                if ($this->getFieldByKey($key, $name)) {
                    continue;
                }

                if (array_key_exists('role', $metaOptions)) {
                    $role = $metaOptions['role'];
                }

                $this->createMeta($key, $name, $metaOptions['options']['label'], $metaOptions['type'], null, $role);
            }
        }
    }

    protected function generateArray($entities)
    {
        $output = array();

        foreach ($entities as $entity) {
            $output[$entity->getField()] = $entity->getValue();
        }

        return $output;
    }
}