<?php

namespace Push\MetaManagerBundle\Form\Type;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class RichTextType extends CKEditorType
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'ckeditor';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'richtext';
    }
}