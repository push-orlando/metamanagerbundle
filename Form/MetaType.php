<?php

namespace Push\MetaManagerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MetaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $options['data'];
        
        if ($data->getType() == "collection") {
            $builder
                ->add('value', 'collection', array(
                    'type' => 'text',
                    'attr'  => array('class' => 'collection'),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'options' => array(
                        'required' => false,
                        'label_attr' => array(
                            'style' => 'display:none'
                        )
                    )
                ))
            ;
        } else {
            $builder
                ->add('value', $data->getType())
            ;
        }
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Push\MetaManagerBundle\Entity\Meta'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'push_metamanagerbundle_meta';
    }
}
