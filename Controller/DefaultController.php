<?php

namespace Push\MetaManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('PushMetaManagerBundle:Default:index.html.twig', array('name' => $name));
    }
}
