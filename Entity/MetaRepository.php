<?php

namespace Push\MetaManagerBundle\Entity;

use Doctrine\ORM\EntityRepository;

class MetaRepository extends EntityRepository
{
    public function getAllKeys()
    {
        return $this->createQueryBuilder('m')
            ->select('DISTINCT(m.metakey)')
            ->getQuery()
            ->getScalarResult();
    }

    public function getFieldsByKey($key, $requiredRole)
    {
        $result = $this->createQueryBuilder('m')
            ->where('m.metakey = :key')
            ->setParameter(':key', $key);
        
        if ($requiredRole != 'all') 
        {
            $result = $result->andWhere('m.requiredRole = :requiredRole OR m.requiredRole IS NULL')
                             ->setParameter(':requiredRole', $requiredRole);
        }

        $result = $result->getQuery()
                         ->getResult();

        return $result;
    }
}